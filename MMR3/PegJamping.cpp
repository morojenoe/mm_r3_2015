#include <string>
#include <vector>

using namespace std;

class PegJumping
{
public:
	vector<string> getMoves(vector<int> pegValue, vector<string> board)
	{
		return vector<string>();
	}
};

#ifdef MY_DEBUG
int main()
{
	int m;
	scanf("%d", &m);
	vector<int> pegValue(m);
	for(int i = 0; i < m; i++)
		scanf("%d", &pegValue[i]);
	
	int n;
	scanf("%d\n", &n);
	vector<string> board(n);
	PegJumping pegJumping;
	vector<string> ret = pegJumping.getMoves(pegValue, board);
	printf("%d\n", (int)ret.size());
	for(int i = 0; i < (int)ret.size(); i++)
		printf("%s\n", ret[i].c_str());

	return 0;
}
#endif
